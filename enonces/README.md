# Enoncés

## Générer les énoncés

### Générer pour les participants en PDF

Sur le projet `java-tp-enonces`, exécuter la commande Maven :
```
mvn post-site
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `java-tp-enonces`.

### Générer pour le formateur en PDF

Sur le projet `java-tp-enonces`, exécuter la commande Maven :
```
mvn post-site -Pformateur
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `java-tp-enonces`.


### Générer pour les participants en HTML

Sur le projet `java-tp-enonces`, exécuter la commande Maven :
```
mvn post-site -Phtml
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `java-tp-enonces`.

### Générer pour le formateur en HTML

Sur le projet `java-tp-enonces`, exécuter la commande Maven :
```
mvn post-site -Phtml,formateur
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `java-tp-enonces`.



## Ajouter des informations à destination du formateur

Pour ajouter une section qui ne sera visible que pour le formateur, il suffit d'encadrer le contenu comme ceci :
```
ifdef::notesformateur[]
Votre contenu à destination du formateur
endif::[]
```

Une convention a été mise en place pour que ce contenu soit rapidement identifiable par le formateur (utilisation d'une icône et d'un en-tête) :
```
ifdef::notesformateur[]
[TIP]
.Notes pour le formateur
====
Votre contenu à destination du formateur
====
endif::[]
```


