package fr.sii.formation.java;

public class Main {

    public static void main(String[] args) {

        System.out.println("Début du quizz !");

        // Set given answers and corrects answers
        float[] correctAnswerList = {8, 4, 1547};
        float[] answerList = {8, 4, 1543};

        // Error authorized
        int errorMarge = 5;

        // Calculate result
        boolean areAllCorrect = (correctAnswerList[0] + errorMarge >= answerList[0]) && (correctAnswerList[0] - errorMarge <= answerList[0]);
        areAllCorrect &= (correctAnswerList[1] + errorMarge >= answerList[1]) && (correctAnswerList[1] - errorMarge <= answerList[1]);
        areAllCorrect &= (correctAnswerList[2] + errorMarge >= answerList[2]) && (correctAnswerList[2] - errorMarge <= answerList[2]);

        // Log question count
        System.out.println("Question count: " + answerList.length);

        // Log player result
        System.out.println("Correct: " + (areAllCorrect ? 'Y' : 'N'));

    }
}
